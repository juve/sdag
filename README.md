sDAG
====

This tool can be used to generate normal Condor DAGMan DAGs from simple DAG
format (sDAG) DAGs. This project was an experiment to try and simplify the
process of creating workflows for DAGMan.

Instead of specifying a submit script for every job, this tool generates one
based on the contents of the sDAG.

Here's an example sDAG containing one job (`simple.sdag`):

```
JOB foo /bin/date
```

The sdag-compile tool will generate a DAGMan DAG and a .sub script for that job.
The DAG file (`simple.dag`) looks like this:

```
JOB foo foo.sub
```

And the `foo.sub` file looks like this:

foo.sub>>
should_transfer_files = YES
requirements = (Arch == Arch) && (OpSys == OpSys) && (Disk != 0) && (Memory != 0)
periodic_remove = (NumSystemHolds > 3)
transfer_executable = false
transfer_error = true
universe = vanilla
stream_output = false
transfer_output = true
stream_error = false
copy_to_spool = false
notification = NEVER
when_to_transfer_output = ON_EXIT
periodic_release = (NumSystemHolds <= 3)
environment = 
executable = /bin/date
output = /Users/juve/Workspace/sdag/examples/simple/foo.out
error = /Users/juve/Workspace/sdag/examples/simple/foo.err
log = /Users/juve/Workspace/sdag/examples/simple/simple.log
initialdir = /Users/juve/Workspace/sdag/examples/simple
queue 1
<<foo.sub

As you can see, a very little sDAG can be used to generate a lot of the 
boilerplate that is required in a submit script.

You can specify dependencies like this:

```
JOB foo /bin/date
JOB bar /bin/date
PARENT foo CHILD bar
```

Dependencies can be defined at any point after both the parent and child
have been defined--they do not need to go only at the end.

You can override any parameter that can be placed in the submit script
by inserting it before the job definition. For example, if you want a
local universe job:

```
UNIVERSE local
JOB localjob /bin/hostname
UNIVERSE vanilla
JOB remotejob /bin/hostname
```

In this case, localjob will run int the local universe, and remotejob
will run in the vanilla universe.

You can also set environment variables for your jobs:

```
ENV PATH /bin:/usr/bin
ENV PEGASUS_HOME /usr/local/pegasus
JOB env /usr/bin/env
```

Alternatively, you could set the ENVIRONMENT parameter for the submit
script directly:

```
ENVIRONMENT = PATH=/bin:/usr/bin;PEGASUS_HOME=/usr/local/pegasus
```

Use one method or the other though. If you define both ENV and ENVIRONMENT, 
then ENVIRONMENT wins.

You can also use pre and post scripts:

```
JOB foo /bin/date
SCRIPT PRE /bin/echo pre
SCRIPT POST /bin/echo post
```

If you want to set retry, you can use the 'RETRY' keyword:

```
RETRY 3
JOB foo /bin/date
JOB bar /bin/date
```

That will produce a DAG that looks like this:

```
JOB foo foo.sub
RETRY foo 3
JOB bar bar.sub
RETRY bar 3
```

